package com.ssttevee.taskmanagerapp;

import java.util.ArrayList;

import com.ssttevee.taskmanagerapp.tasks.ServerCommunicator;
import com.ssttevee.taskmanagerapp.tasks.Task;
import com.ssttevee.taskmanagerapp.tasks.TasksSQLiteOpenHelper;

import android.app.Application;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import static com.ssttevee.taskmanagerapp.tasks.TasksSQLiteOpenHelper.*;

public class TaskManagerApplication extends Application {

	private ArrayList<Task> currentTasks;
	private Task currentTask;
	private SQLiteDatabase database;
	private ArrayList<Integer> ids = new ArrayList<Integer>();
	private boolean premium;

	@Override
	public void onCreate() {
		super.onCreate();
		TasksSQLiteOpenHelper helper = new TasksSQLiteOpenHelper(this);
		database = helper.getWritableDatabase();
		if (null == currentTasks) {
			loadTasks();
		}
		premium = getSharedPreferences("mclprefs", MODE_PRIVATE).getBoolean(
				"EXTRA_FEATURES", false);
	}

	public void loadTasks() {
		currentTasks = new ArrayList<Task>();
		Cursor tasksCursor = database.query(SERVERS_TABLE,
				new String[] { SERV_ID, // 0
						SERV_NAME, // 1
						SERV_HOST, // 2
						SERV_MOTD, // 3
						SERV_PMAX, // 4
						SERV_PMIN, // 5
						SERV_PLAYERS, // 6
						SERV_PING, // 7
						SERV_VERSION, // 8
						SERV_TONLINE, // 9
						SERV_TCHECKED, // 10
						SERV_GS4, // 11
						SERV_STATUS, // 12
						SERV_PORT, // 13
						SERV_MQ // 14
				}, null, null, null, null,
				String.format("%s, %s", SERV_ID, SERV_NAME));
		tasksCursor.moveToFirst();
		Task t;
		if (!tasksCursor.isAfterLast()) {
			int i = 0;
			int j = 0;
			do {
				long id = tasksCursor.getLong(0);
				String name = tasksCursor.getString(1);
				String host = tasksCursor.getString(2);
				String motd = tasksCursor.getString(3);
				int pmax = tasksCursor.getInt(4);
				int ponline = tasksCursor.getInt(5);
				String players = tasksCursor.getString(6);
				int ping = tasksCursor.getInt(7);
				String version = tasksCursor.getString(8);
				int tonline = tasksCursor.getInt(9);
				int tchecked = tasksCursor.getInt(10);
				int gs4Port = tasksCursor.getInt(11);
				int port = tasksCursor.getInt(13);
				int minequery_port = tasksCursor.getInt(14);
				String boolValue = tasksCursor.getString(12);
				boolean online = Boolean.parseBoolean(boolValue);
				if (port == 0) {
					port = Integer.parseInt(host.split(":")[1]);
					host = host.split(":")[0];
				}
				t = new Task(id, name, host, motd, pmax, ponline, players,
						ping, version, tonline, tchecked, online);
				t.setGs4Port(gs4Port);
				t.setPort(port);
				t.setMinequeryPort(minequery_port);
				currentTasks.add(t);

				for (int k = j; k < (int) id; k++) {
					ids.add(0);
				}

				ids.add(i);
				i++;
			} while (tasksCursor.moveToNext());
		}
		tasksCursor.close();
	}

	public void setCurrentTasks(ArrayList<Task> currentTasks) {
		this.currentTasks = currentTasks;
	}

	public ArrayList<Task> getCurrentTasks() {
		return currentTasks;
	}

	public void addTask(Task t) {
		assert (t != null);

		(new ServerCommunicator(this)).postAddedServer(t);

		ContentValues values = new ContentValues();
		values.put(SERV_NAME, t.getName());
		values.put(SERV_HOST, t.getHost());
		values.put(SERV_PORT, t.getPort());
		values.put(SERV_GS4, t.getGs4Port());
		values.put(SERV_MQ, t.getMinequeryPort());
		values.put(SERV_MOTD, t.getMotd());
		values.put(SERV_PMAX, t.getMaxPlayers());
		values.put(SERV_PMIN, t.getOnlinePlayers());
		values.put(SERV_PLAYERS, t.getPlayers());
		values.put(SERV_PING, t.getLatency());
		values.put(SERV_VERSION, t.getVersion());
		values.put(SERV_TONLINE, t.getTimesOnline());
		values.put(SERV_TCHECKED, t.getTimesChecked());
		values.put(SERV_STATUS, Boolean.toString(t.isOnline()));

		t.setId(database.insert(SERVERS_TABLE, null, values));
		currentTasks.add(t);
	}

	public void saveTask(Task t) {
		assert (t != null);

		ContentValues values = new ContentValues();
		values.put(SERV_NAME, t.getName());
		values.put(SERV_HOST, t.getHost());
		values.put(SERV_PORT, t.getPort());
		values.put(SERV_GS4, t.getGs4Port());
		values.put(SERV_MQ, t.getMinequeryPort());
		values.put(SERV_MOTD, t.getMotd());
		values.put(SERV_PMAX, t.getMaxPlayers());
		values.put(SERV_PMIN, t.getOnlinePlayers());
		values.put(SERV_PLAYERS, t.getPlayers());
		values.put(SERV_PING, t.getLatency());
		values.put(SERV_VERSION, t.getVersion());
		values.put(SERV_TONLINE, t.getTimesOnline());
		values.put(SERV_TCHECKED, t.getTimesChecked());
		values.put(SERV_STATUS, Boolean.toString(t.isOnline()));

		String where = String.format("%s = ?", SERV_ID);
		database.update(SERVERS_TABLE, values, where, new String[] { t.getId()
				+ "" });
	}

	public void saveTask(Task t, boolean doSet) {
		assert (t != null);
		saveTask(t);
		if (doSet) {
			currentTasks.set(ids.get((int) t.getId()), t);
		}
	}

	public void delTasks(Long id) {
		String where = String.format("%s in (%s)", SERV_ID, id);
		database.delete(SERVERS_TABLE, where, null);
		currentTasks.remove(id);
	}

	public Task getTaskById(int id) {
		String where = String.format("%s = ?", SERV_ID);
		Cursor tasksCursor = database.query(SERVERS_TABLE,
				new String[] { SERV_NAME, // 0
						SERV_HOST, // 1
						SERV_MOTD, // 2
						SERV_PMAX, // 3
						SERV_PMIN, // 4
						SERV_PLAYERS, // 5
						SERV_PING, // 6
						SERV_VERSION, // 7
						SERV_TONLINE, // 8
						SERV_TCHECKED, // 9
						SERV_GS4, // 10
						SERV_STATUS, // 11
						SERV_PORT, // 12
						SERV_MQ // 13
				}, where, new String[] { id + "" }, null, null,
				String.format("%s, %s", SERV_ID, SERV_NAME));
		tasksCursor.moveToFirst();
		Task t = new Task(id, tasksCursor.getString(0),
				tasksCursor.getString(1), tasksCursor.getString(2),
				tasksCursor.getInt(3), tasksCursor.getInt(4),
				tasksCursor.getString(5), tasksCursor.getInt(6),
				tasksCursor.getString(7), tasksCursor.getInt(8),
				tasksCursor.getInt(9), Boolean.parseBoolean(tasksCursor
						.getString(11)));
		t.setGs4Port(tasksCursor.getInt(10));
		t.setPort(tasksCursor.getInt(12));
		t.setMinequeryPort(tasksCursor.getInt(13));
		tasksCursor.close();
		return t;
	}

	public Task getCurrentTask() {
		return currentTask;
	}

	public void setCurrentTask(Task currentTask) {
		this.currentTask = currentTask;
	}

	public boolean isPremium() {
		return premium;
	}

	public void setPremium(boolean premium) {
		getSharedPreferences("mclprefs", MODE_PRIVATE).edit()
				.putBoolean("EXTRA_FEATURES", true).commit();
		this.premium = premium;
	}
}
