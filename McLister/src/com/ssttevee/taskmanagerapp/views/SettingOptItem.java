package com.ssttevee.taskmanagerapp.views;

import com.ssttevee.taskmanagerapp.R;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SettingOptItem extends RelativeLayout {

	private TextView optname;
	private TextView optval;
	private TextView optreq;

	public SettingOptItem(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		optname = (TextView)findViewById(R.id.opt_name);
		optval = (TextView)findViewById(R.id.opt_value);
		optreq = (TextView)findViewById(R.id.required_note);
	}

	public void setValue(String name, String val) {
		optname.setText(name);
		if(val == "") {
			val = "Nothing Here!";
		}
		optval.setText(val);
	}

	public void setRequired(String str) {
		if(str == "false") {
			this.optreq.setVisibility(8);
		} else {
			this.optreq.setVisibility(0);
		}
	}

}
