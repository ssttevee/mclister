package com.ssttevee.taskmanagerapp.views;

import com.ssttevee.taskmanagerapp.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class plistItem extends RelativeLayout {

	private TextView pname;
	private ImageView pface;

	public plistItem(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		pname = (TextView)findViewById(R.id.player_name);
		pface = (ImageView)findViewById(R.id.player_face);
	}

	public void setName(String name, Bitmap bm) {
		pname.setText(name);
	    pface.setImageBitmap(bm);
	}
}
