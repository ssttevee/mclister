package com.ssttevee.taskmanagerapp.views;

import com.ssttevee.taskmanagerapp.R;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ServerInfoItem extends RelativeLayout {

	private TextView title;
	private TextView value;

	public ServerInfoItem(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		title = (TextView)findViewById(R.id.info_type);
		value = (TextView)findViewById(R.id.info_data);
	}

	public void setValue(String name, String val) {
		title.setText(name);
		value.setText(val);
	}


}
