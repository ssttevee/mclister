package com.ssttevee.taskmanagerapp.views;

import com.ssttevee.taskmanagerapp.R;
import com.ssttevee.taskmanagerapp.tasks.Task;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.util.AttributeSet;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class TaskListItem extends RelativeLayout {
	
	private Task task;
	private TextView serverName;
	private TextView serverMotd;
	private TextView serverSlot;
	private TextView serverAddr;
	private ImageView status;
	private ImageView pingBars;
	private ImageView statusUnder;
	
	public TaskListItem(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		serverName = (TextView) findViewById(R.id.server_name);
		serverMotd = (TextView) findViewById(R.id.server_motd);
		serverSlot = (TextView) findViewById(R.id.server_slots);
		serverAddr = (TextView) findViewById(R.id.server_address);
		status = (ImageView) findViewById(R.id.status_identifier);
		statusUnder = (ImageView) findViewById(R.id.status_identifier_under);
		pingBars = (ImageView) findViewById(R.id.ping_bars);
	}
	
	public Task getTask() {
		return task;
	}
	
	public void setFont(Typeface tf) {
		serverName.setTypeface(tf);
		serverMotd.setTypeface(tf);
		serverSlot.setTypeface(tf);
		serverAddr.setTypeface(tf);
	}

	public void setTask(Task task) {
		this.task = task;
		serverName.setText(task.getName());
		serverMotd.setText(task.getMotd());
		serverSlot.setText(task.getOnlinePlayers() + "/" + task.getMaxPlayers());
		serverAddr.setText(task.getHost());
    	pingBars.setBackgroundResource(R.drawable.bars_0);
		if(task.isOnline() == true) {
			status.setBackgroundResource(R.drawable.status_green);
		} else {
			status.setBackgroundResource(R.drawable.status_red);
		}
		serverMotd.setTextColor((task.isOnline() ? Color.rgb(170, 170, 170) : Color.rgb(221, 32, 32)));
	    setBars((task.isOnline() ? task.getLatency() : -1));
	}

	@Override
	public void setSelected(boolean isSelected) {
		if(isSelected) {
			setBackgroundResource(R.drawable.server_selected);
		} else {
			setBackgroundResource(R.drawable.server_list);
		}
	}
	
	public void startPolling() {
		Animation fadeOut = new AlphaAnimation(1.0f, 0.0f);
		fadeOut.setDuration(500);
		
		statusUnder.setBackgroundResource(R.drawable.status_white);
	    status.startAnimation(fadeOut);
		status.setBackgroundResource(R.drawable.status_white);
		status.setAlpha(255);
	    
		pingBars.setBackgroundResource(R.anim.polling_bars);
		
		AnimationDrawable frameAnimation = (AnimationDrawable) pingBars.getBackground();
		frameAnimation.start();
	}
	
	public void finishPolling() {
		Animation fadeIn = new AlphaAnimation(0.0f, 1.0f);
		fadeIn.setDuration(500);
		
		statusUnder.setBackgroundResource(R.drawable.status_white);
		status.setAlpha(0);
		if(task.isOnline()) {
			status.setBackgroundResource(R.drawable.status_green);
		} else {
			status.setBackgroundResource(R.drawable.status_red);
		}
	    status.startAnimation(fadeIn);
	    setBars((task.isOnline() ? task.getLatency() : -1));
	}
	
	public void setBars(int lag) {
        if (lag < 0) {
        	pingBars.setBackgroundResource(R.drawable.bars_0);
        } else if (lag < 150) {
        	pingBars.setBackgroundResource(R.drawable.bars_5);
        } else if (lag < 300) {
        	pingBars.setBackgroundResource(R.drawable.bars_4);
        } else if (lag < 600) {
        	pingBars.setBackgroundResource(R.drawable.bars_3);
        } else if (lag < 1000) {
        	pingBars.setBackgroundResource(R.drawable.bars_2);
        } else {
        	pingBars.setBackgroundResource(R.drawable.bars_1);
        }
	}
}
