package com.ssttevee.taskmanagerapp;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;

import com.flurry.android.FlurryAdSize;
import com.flurry.android.FlurryAgent;
import com.ssttevee.taskmanagerapp.R;
import com.ssttevee.taskmanagerapp.tasks.ServerCommunicator;
import com.ssttevee.taskmanagerapp.tasks.ServerQuery;
import com.ssttevee.taskmanagerapp.tasks.Task;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class AddServerActivity extends Activity {

	private boolean changesPending;
	private boolean nameEmpty;
	private boolean online;
	private boolean qonline;
	private boolean edit = true;
	private int max_players;
	private int online_players;
	private int latency;
	private int gs4_port;
	private int times_online;
	private int times_checked;
	private int returnMsg;
	private long id;
	private String name;
	private String host;
	private String motd;
	private String players;
	private String version;
	private Button addButton;
	private Button cancelButton;
	private AlertDialog unsavedChangedDialog;
	private AlertDialog testDialog;
	private Task t;
	private ServerQuery sq = new ServerQuery();
	private ProgressBar loadSpinner;
	private TextView importantNote;
	private EditText fieldSName;
	private EditText fieldSAddr;
	private EditText fieldSPort;
	private EditText fieldQPort;
	private EditText fieldMQPort;
	private int port;
	private int mqport;
	private ImageView resetButton;
	private ImageView importButton;
	private String[] web_lists = { "minestatus.net", "minecraftservers.org",
			"minecraftservers.net", "mc-index.com" };
	private AlertDialog.Builder builder;
	private ArrayAdapter<String> spinnerArrayAdapter;
	private SharedPreferences prefs;
	private TaskManagerApplication mApp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_add_task);
		viewSetup();

		mApp = (TaskManagerApplication) getApplication();
	}

	@Override
	protected void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(this, "HKF23YTGHJ6THQQZNYDZ");
		FlurryAgent.initializeAds(this);
		// FlurryAgent.enableTestAds(true);
	}

	@Override
	protected void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(this);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cancel();
			return false;
		} else if (keyCode == KeyEvent.KEYCODE_MENU) {
			Task t = new Task(0, "", "mc.nyancraft.com:25565", "", 0, 0, "", 0,
					"", 0, 0, false);
			t.setMinequeryPort(25566);
			try {
				sq.queryMq(t);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return super.onKeyDown(keyCode, event);
	}

	private void viewSetup() {
		Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/volter.ttf");
		fieldSName = (EditText) findViewById(R.id.sname_field);
		fieldSAddr = (EditText) findViewById(R.id.saddr_field);
		fieldSPort = (EditText) findViewById(R.id.sport_field);
		fieldQPort = (EditText) findViewById(R.id.qport_field);
		fieldMQPort = (EditText) findViewById(R.id.mqport_field);
		fieldSName
				.addTextChangedListener(new TextWatch(0, fieldSName.getText()));
		fieldSAddr
				.addTextChangedListener(new TextWatch(1, fieldSAddr.getText()));
		fieldSPort
				.addTextChangedListener(new TextWatch(2, fieldSPort.getText()));
		fieldQPort
				.addTextChangedListener(new TextWatch(3, fieldQPort.getText()));
		fieldMQPort.addTextChangedListener(new TextWatch(4, fieldMQPort
				.getText()));
		addButton = (Button) findViewById(R.id.add_task_btn);
		resetButton = (ImageView) findViewById(R.id.reset_btn);
		importButton = (ImageView) findViewById(R.id.import_btn);
		cancelButton = (Button) findViewById(R.id.cancel_add_task_btn);
		loadSpinner = (ProgressBar) findViewById(R.id.at_spinner);
		importantNote = (TextView) findViewById(R.id.important_note);
		fieldSAddr.setInputType(InputType.TYPE_TEXT_VARIATION_URI);
		loadSpinner.setVisibility(View.INVISIBLE);
		addButton.setTypeface(tf);
		cancelButton.setTypeface(tf);
		cancelButton.setEnabled(false);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			id = extras.getLong("id");
			name = extras.getString("name");
			host = extras.getString("host");
			motd = extras.getString("motd");
			port = extras.getInt("port");
			mqport = extras.getInt("mqport");
			max_players = extras.getInt("max_players");
			online_players = extras.getInt("online_players");
			players = extras.getString("players");
			latency = extras.getInt("latency");
			gs4_port = extras.getInt("gs4_port");
			version = extras.getString("version");
			times_online = extras.getInt("times_online");
			times_checked = extras.getInt("times_checked");
			online = extras.getBoolean("online");
		}
		spinnerArrayAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_dropdown_item, web_lists);
		builder = new AlertDialog.Builder(this);
		builder.setTitle("Choose a Service").setAdapter(spinnerArrayAdapter,
				new DialogInterface.OnClickListener() {
					private EditText sidBox;

					@Override
					public void onClick(DialogInterface dialog, int item) {

						sidBox = new EditText(AddServerActivity.this);
						sidBox.setInputType(InputType.TYPE_CLASS_NUMBER);
						final int sitemr = item;

						AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
								AddServerActivity.this);
						alertDialogBuilder.setView(sidBox);
						alertDialogBuilder
								.setMessage("Please Enter the Server ID from the website.");
						alertDialogBuilder
								.setCancelable(false)
								.setPositiveButton("OK",
										new DialogInterface.OnClickListener() {
											@Override
											public void onClick(
													DialogInterface dialog,
													int id) {
												SudoMethod sm = new SudoMethod();
												sm.execute("import", sitemr
														+ "", sidBox.getText()
														.toString());
											}
										})
								.setNegativeButton("Cancel",
										new DialogInterface.OnClickListener() {
											@Override
											public void onClick(
													DialogInterface dialog,
													int id) {
												dialog.cancel();
											}
										});

						// create alert dialog
						AlertDialog alertDialog = alertDialogBuilder.create();

						// show it
						alertDialog.show();
					}
				});
		importantNote
				.setText("NOTE: Query Port is not the Server Port.  If you don't know what it is, leave it blank.  It will cause connection failure.  Also, some mobile service providers block the protocol that is used.  This will also cause connection failure");
		addButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (online == true) {
					addTask();
				}
			}
		});
		cancelButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				check();
			}
		});

		importButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mApp.isPremium()) {
					builder.create().show();
				} else {
					// new
					// AlertDialog.Builder(AddServerActivity.this).setTitle("Error").setMessage("Available only after upgrade").create().show();
					Toast.makeText(getBaseContext(),
							"Available only after upgrade", Toast.LENGTH_SHORT)
							.show();
				}
			}
		});
		importButton.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				Toast.makeText(getBaseContext(), "Import from web list",
						Toast.LENGTH_SHORT).show();
				return true;
			}
		});

		resetButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Toast.makeText(getBaseContext(), "Reset Form.  Hold to reset.",
						Toast.LENGTH_SHORT).show();
			}
		});
		resetButton.setOnLongClickListener(new View.OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				resetForm();
				return true;
			}
		});

		if (id == 65535) {
			edit = false;
		} else {
			fieldSName.setText(name);
			fieldSAddr.setText(host);
			fieldSPort.setText(port + "");
			fieldQPort.setText((gs4_port == 0 ? "" : gs4_port + ""));
			fieldMQPort.setText((mqport == 0 ? "" : mqport + ""));

			((TextView) findViewById(R.id.task_title)).setText("Edit " + name);
			addButton.setText("Save Changes");

			TaskManagerApplication app = (TaskManagerApplication) getApplication();
			t = app.getTaskById((int) id);
			if (gs4_port > 0 && online) {
				returnMsg = 4;
			} else if (online) {
				returnMsg = 1;
			}
		}

		csUpdateUI();
	}

	private void resetForm() {
		if (edit) {
			fieldSName.setText(name);
			fieldSAddr.setText(host);
			fieldSPort.setText(port + "");
			fieldQPort.setText((gs4_port == 0 ? "" : gs4_port + ""));
			fieldMQPort.setText((mqport == 0 ? "" : mqport + ""));
		} else {
			fieldSName.setText("");
			fieldSAddr.setText("");
			fieldSPort.setText("");
			fieldQPort.setText("");
			fieldMQPort.setText("");
		}
	}

	protected void addTask() {
		int port = Integer.parseInt(fieldSPort.getText().toString());
		int qport = 0;
		int mqport = 0;
		String sname = "Minecraft Server";
		TaskManagerApplication tma = (TaskManagerApplication) getApplication();

		if (fieldQPort.getText().length() > 0) {
			qport = Integer.parseInt(fieldQPort.getText().toString());
		}
		if (fieldMQPort.getText().length() > 0) {
			mqport = Integer.parseInt(fieldMQPort.getText().toString());
		}
		if (fieldSName.getText().length() > 0) {
			sname = fieldSName.getText().toString();
		}

		if (edit == true) {
			t.setId(id);
			t.setName(sname);
			t.setPort(port);
			t.setGs4Port(qport);
			t.setMinequeryPort(mqport);
			tma.saveTask(t);
			finish();
		} else {
			t.setName(sname);
			t.setPort(port);
			t.setGs4Port(qport);
			t.setMinequeryPort(mqport);
			tma.addTask(t);
			if (!mApp.isPremium())
				FlurryAgent.getAd(this, "ViewInfoInterstitial",
						(ViewGroup) findViewById(R.id.whole_view),
						FlurryAdSize.FULLSCREEN, 5000);
			finish();
		}
		overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
	}

	protected void csUpdateUI() {
		if (returnMsg == 0 || returnMsg == 2 || returnMsg == 3) {
			addButton.setEnabled(false);
		} else if (returnMsg == 1 || returnMsg == 4) {
			if (qonline == false && fieldQPort.getText().length() > 0) {
				addButton.setEnabled(false);
			} else if (qonline == true) {
				addButton.setEnabled(true);
			} else {
				addButton.setEnabled(true);
			}
			if (online == true) {
				addButton.setEnabled(true);
			} else if (online == false) {
				addButton.setEnabled(true);
			}
		}
	}

	protected void check() {
		if (isInternetAvailable()) {
			boolean saddr = false;
			boolean sport = false;
			boolean mqport = false;
			boolean qport = false;
			SudoMethod sm = new SudoMethod();
			if (fieldSAddr.getText().length() <= 0) {
				Toast.makeText(getBaseContext(), "Server address is missing",
						Toast.LENGTH_SHORT).show();
			} else if (!sq.isAddrValid()) {
				Toast.makeText(getBaseContext(), "Unable to find host",
						Toast.LENGTH_SHORT).show();
			} else {
				saddr = true;
			}

			if (fieldSPort.getText().length() <= 0) {
				Toast.makeText(getBaseContext(), "Server Port is missing",
						Toast.LENGTH_SHORT).show();
			} else if (Integer.parseInt(fieldSPort.getText().toString()) > 65535) {
				Toast.makeText(getBaseContext(), "Query Port is too big",
						Toast.LENGTH_SHORT).show();
			} else if (!(Integer.parseInt(fieldSPort.getText().toString()) > 0)) {
				Toast.makeText(getBaseContext(), "Query Port is too small",
						Toast.LENGTH_SHORT).show();
			} else {
				sport = true;
			}

			if (fieldQPort.getText().length() <= 0) {
			} else if (Integer.parseInt(fieldQPort.getText().toString()) > 65535) {
				Toast.makeText(getBaseContext(), "Query Port is too big",
						Toast.LENGTH_SHORT).show();
			} else if (!(Integer.parseInt(fieldQPort.getText().toString()) > 0)) {
				Toast.makeText(getBaseContext(), "Query Port is too small",
						Toast.LENGTH_SHORT).show();
			} else {
				qport = true;
			}

			if (fieldMQPort.getText().length() <= 0) {
			} else if (Integer.parseInt(fieldMQPort.getText().toString()) > 65535) {
				Toast.makeText(getBaseContext(), "MineQuery Port is too big",
						Toast.LENGTH_SHORT).show();
			} else if (!(Integer.parseInt(fieldMQPort.getText().toString()) > 0)) {
				Toast.makeText(getBaseContext(), "MineQuery Port is too small",
						Toast.LENGTH_SHORT).show();
			} else {
				mqport = true;
			}

			if (saddr == true) {
				if (qport == true) {
					sm.execute("query");
					cancelButton.setEnabled(false);
				} else if (mqport == true) {
					sm.execute("mq");
					cancelButton.setEnabled(false);
				} else if (sport == true) {
					sm.execute("poll");
					cancelButton.setEnabled(false);
				}
			}

		} else {
			Toast.makeText(getBaseContext(), "Network Unavailable",
					Toast.LENGTH_SHORT).show();
		}

	}

	protected void cancel() {
		if (changesPending) {
			unsavedChangedDialog = new AlertDialog.Builder(this)
					.setTitle(R.string.unsaved_changed_title)
					.setMessage("Do you really want to lose this data?")
					.setNeutralButton(R.string.discard,
							new AlertDialog.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									finish();
									overridePendingTransition(
											R.anim.in_from_right,
											R.anim.out_to_left);
								}
							})
					.setNegativeButton(R.string.cancel,
							new AlertDialog.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									unsavedChangedDialog.cancel();
								}
							}).create();
			unsavedChangedDialog.show();
		} else {
			finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
		}
	}

	protected void sendPopup(String title, String msg) {
		testDialog = new AlertDialog.Builder(this)
				.setTitle(title)
				.setMessage(msg)
				.setNeutralButton("Alright...",
						new AlertDialog.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								testDialog.cancel();
							}
						}).create();
		testDialog.show();
	}

	public boolean isInteger(String input) {
		try {
			Integer.parseInt(input);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean isInternetAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		if (activeNetworkInfo != null) {
			return activeNetworkInfo.isConnected();
		} else {
			return false;
		}
	}

	private boolean IsMatch(String s, String pattern) {
		try {
			Pattern patt = Pattern.compile(pattern);
			Matcher matcher = patt.matcher(s);
			return matcher.matches();
		} catch (RuntimeException e) {
			return false;
		}
	}

	public void showSpinner() {
		loadSpinner.setVisibility(View.VISIBLE);
		resetButton.setVisibility(View.INVISIBLE);
		importButton.setVisibility(View.INVISIBLE);
	}

	public void disableAllFields() {
		fieldSName.setEnabled(false);
		fieldSAddr.setEnabled(false);
		fieldSPort.setEnabled(false);
		fieldQPort.setEnabled(false);
		fieldMQPort.setEnabled(false);
	}

	public void enableAllFields() {
		fieldSName.setEnabled(true);
		fieldSAddr.setEnabled(true);
		fieldSPort.setEnabled(true);
		fieldQPort.setEnabled(true);
		fieldMQPort.setEnabled(true);
	}

	public void hideSpinner() {
		loadSpinner.setVisibility(View.INVISIBLE);
		resetButton.setVisibility(View.VISIBLE);
		importButton.setVisibility(View.VISIBLE);
	}

	public boolean isNameEmpty() {
		return nameEmpty;
	}

	public void setNameEmpty(boolean nameEmpty) {
		this.nameEmpty = nameEmpty;
	}

	public class SudoMethod extends AsyncTask<String, Integer, String> {
		private String[] messages = new String[] { "Connection Refused",
				"Connection Successful", "Unable to find host",
				"Query Attempt Failed", "Query Request Successful",
				"An Unknown Error Has Occurred" };
		private String func;

		public SudoMethod() {
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showSpinner();
			disableAllFields();
		}

		@Override
		protected String doInBackground(String... params) {
			if (params[0] == "poll") {
				poll();
			} else if (params[0] == "query") {
				query();
			} else if (params[0] == "mq") {
				minequery();
			} else if (params[0] == "import") {
				Map<String, String> articleParams = new HashMap<String, String>();
				articleParams.put("Service", params[1]); // Capture author info
				articleParams.put("ServerId", params[2]); // Capture user status
				FlurryAgent.logEvent("Server_Imported", articleParams, true);
				import_server(params[1], params[2]);
			}
			func = params[0];
			return "All Done!";
		}

		private void import_server(String service, String server) {
			String[] services = { "minestatus", "minecraftservers.org",
					"minecraftservers.net", "mc-index.com" };
			ArrayList<String> serverInfo;
			ServerCommunicator sc = new ServerCommunicator(
					AddServerActivity.this);
			serverInfo = sc.requestImport(services[Integer.parseInt(service)],
					server);

			if (serverInfo.isEmpty()) {
				publishProgress(-5);
			} else {
				name = serverInfo.get(0);
				host = serverInfo.get(1);
				port = Integer.parseInt(serverInfo.get(2));
				times_online = Integer.parseInt(serverInfo.get(3));
				times_checked = Integer.parseInt(serverInfo.get(4));
				publishProgress(7);
				// poll();
			}
		}

		private void minequery() {
			try {
				Task tt = new Task(fieldSAddr.getText().toString());
				tt.setPort(Integer.parseInt(fieldSPort.getText().toString()));
				tt.setMinequeryPort(Integer.parseInt(fieldMQPort.getText()
						.toString()));
				t = updateTask(t, sq.queryMq(tt));
				returnMsg = 4;
				online = true;
			} catch (UnknownHostException e) {
				returnMsg = 2;
			} catch (IOException e) {
				returnMsg = 0;
			} catch (JSONException e) {
				returnMsg = 5;
			}
		}

		private void poll() {
			try {
				Task tt = new Task(fieldSAddr.getText().toString());
				tt.setPort(Integer
						.parseInt((fieldSPort.getText().toString() != "" ? fieldSPort
								.getText().toString() : port + "")));
				t = updateTask(t, sq.pollServer(tt));
				t.setTimesOnline(t.getTimesOnline() + times_online);
				t.setTimesChecked(t.getTimesChecked() + times_checked);
				online = true;
				returnMsg = 1;
			} catch (IOException e) {
				online = false;
				returnMsg = 0;
			}
		}

		private void query() {
			try {
				Task tt = new Task(fieldSAddr.getText().toString(),
						Integer.parseInt(fieldQPort.getText().toString()));
				tt.setPort(Integer.parseInt(fieldSPort.getText().toString()));
				t = updateTask(t, sq.queryServer(tt));
				returnMsg = 4;
				qonline = true;
				online = true;
			} catch (UnknownHostException e) {
				online = false;
				qonline = false;
				returnMsg = 2;
			} catch (IOException e) {
				online = false;
				qonline = false;
				returnMsg = 3;
			}
		}

		private Task updateTask(Task t, Task u) {
			if (t == null) {
				t = new Task("No-Name");
			}

			if (u.getTimesChecked() > 0) {
				t.setTimesChecked(u.getTimesChecked());
			}

			if (u.getTimesOnline() > 0) {
				t.setTimesOnline(u.getTimesOnline());
			}

			if (u.getLatency() > 0) {
				t.setLatency(u.getLatency());
			}

			if (u.getMaxPlayers() > 0) {
				t.setMaxPlayers(u.getMaxPlayers());
			}

			if (u.getOnlinePlayers() > 0) {
				t.setOnlinePlayers(u.getOnlinePlayers());
			}

			if (u.getPort() > 0) {
				t.setPort(u.getPort());
			}

			if (u.getPlayers() != null) {
				t.setPlayers(u.getPlayers());
			}

			if (u.getMotd() != null) {
				t.setMotd(u.getMotd());
			}

			if (u.getVersion() != null) {
				t.setVersion(u.getVersion());
			}

			if (u.getHost() != null) {
				t.setHost(u.getHost());
			}

			t.setOnline(u.isOnline());

			return t;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			if (values[0] == -5) {
				new AlertDialog.Builder(AddServerActivity.this)
						.setTitle("Error")
						.setMessage("No server was found with this ID.")
						.create().show();
			} else if (values[0] == 7) {
				fieldSName.setText(name);
				fieldSAddr.setText(host);
				fieldSPort.setText(port + "");
			}
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (func != "import") {
				Toast.makeText(getBaseContext(), messages[returnMsg],
						Toast.LENGTH_SHORT).show();
			}
			hideSpinner();
			enableAllFields();
			csUpdateUI();
		}
	}

	private class TextWatch implements TextWatcher {
		private int id;
		private Editable ed;

		public TextWatch(int id, Editable s) {
			this.id = id;
			this.ed = s;
		}

		@Override
		public void afterTextChanged(Editable s) {
			boolean addr = false;
			boolean port = false;
			boolean qport = false;
			if (this.id == 0) {
				return;
			}
			if (fieldSAddr.getText().length() > 0) {
				addr = true;
			}
			if (fieldSPort.getText().length() > 0) {
				port = true;
			}
			if (fieldQPort.getText().length() > 0) {
				qport = true;
			}

			if (addr && port && this.id > 0) {
				cancelButton.setEnabled(true);
			} else {
				cancelButton.setEnabled(false);
			}

			if (this.id > 0) {
				addButton.setEnabled(false);
			}
			if (this.id == 4 || this.id == 3) {
				if (s.length() > 0) {
					if (this.id == 3) {
						fieldMQPort.setEnabled(false);
					} else {
						fieldQPort.setEnabled(false);
					}
				} else {
					fieldMQPort.setEnabled(true);
					fieldQPort.setEnabled(true);
				}
			}
		}

		@Override
		public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
				int arg3) {
		}

		@Override
		public void onTextChanged(CharSequence arg0, int arg1, int arg2,
				int arg3) {
			if (fieldSAddr.getText().length() > 0 && this.id == 1) {
				String addr = fieldSAddr.getText().toString();
				String iregex = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";
				String hregex = "^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\\-]*[A-Za-z0-9])$";
				if (IsMatch(addr, iregex) || IsMatch(addr, hregex)) {
					Log.d("AddServerActivity", "Validating");
					sq.validateAddress(addr);
				} else {
					Log.d("AddServerActivity", "Invalid URL Form");
				}
			}
		}
	}
}
