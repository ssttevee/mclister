package com.ssttevee.taskmanagerapp;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;

import com.flurry.android.FlurryAdSize;
import com.flurry.android.FlurryAgent;
import com.ssttevee.taskmanagerapp.R;
import com.ssttevee.taskmanagerapp.adapter.PlayersListAdapter;
import com.ssttevee.taskmanagerapp.adapter.ServerInfoAdapter;
import com.ssttevee.taskmanagerapp.tasks.ServerQuery;
import com.ssttevee.taskmanagerapp.tasks.Task;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class ViewInfoActivity extends Activity {

	private TaskManagerApplication mApp;
	private long id;
	private String name;
	private String host;
	private int port;
	private String motd;
	private int max_players;
	private int online_players;
	private String players;
	private int latency;
	private int gs4_port;
	private String version;
	private int times_online;
	private int times_checked;
	private boolean online;
	private ArrayList<String[]> data = new ArrayList<String[]>();
	private ArrayList<Bitmap> bmArray = new ArrayList<Bitmap>();
	private ListView lv;
	private ServerInfoAdapter adapter;
	private double uptime;
	private ServerQuery sq = new ServerQuery();
	private boolean interstitial;
	private int repollfreq = 120; // in Seconds
	private AlertDialog deleteConfirmDialog;
	private ProgressBar loadSpinner;
	private AlertDialog deleteServerDialog;
	private Task t;
	private ProgressDialog progdialog;
	private boolean cancelTask = false;
	private int mqport;
	private SharedPreferences prefs;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_view_info);

		viewSetup();
	}

	@Override
	protected void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(this, "HKF23YTGHJ6THQQZNYDZ");
		FlurryAgent.initializeAds(this);
		// FlurryAgent.enableTestAds(true);
	}

	@Override
	public void onResume() {
		t = mApp.getTaskById((int) id);

		name = t.getName();
		host = t.getHost();
		port = t.getPort();
		mqport = t.getMinequeryPort();
		motd = t.getMotd();
		max_players = t.getMaxPlayers();
		online_players = t.getOnlinePlayers();
		players = t.getPlayers();
		latency = t.getLatency();
		gs4_port = t.getGs4Port();
		version = t.getVersion();
		times_online = t.getTimesOnline();
		times_checked = t.getTimesChecked();
		online = t.isOnline();
		uptime = (double) times_online / (double) times_checked;

		((TextView) findViewById(R.id.header_title)).setText(name);
		changeData(t);
		super.onResume();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (!mApp.isPremium())
				interstitial = FlurryAgent.getAd(this, "ViewInfoInterstitial",
						(ViewGroup) findViewById(R.id.whole_view),
						FlurryAdSize.FULLSCREEN, 5000);
			finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
		} else {
			return super.onKeyDown(keyCode, event);
		}
		return true;
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	private void viewSetup() {
		prefs = this.getSharedPreferences("mclprefs", MODE_PRIVATE);
		boolean isPurchased = prefs.getBoolean("EXTRA_FEATURES", false);
		TextView headerTitle = (TextView) findViewById(R.id.header_title);
		Bundle extras = getIntent().getExtras();
		loadSpinner = (ProgressBar) findViewById(R.id.at_spinner);
		loadSpinner.setVisibility(View.INVISIBLE);

		if (extras != null) {
			id = extras.getLong("id");
		}
		mApp = (TaskManagerApplication) getApplication();
		t = mApp.getTaskById((int) id);

		headerTitle.setText(t.getName());

		// data.add(new String[] {"Server Name", name});
		data.add(new String[] { "Game Address", t.getHost() + ":" + t.getPort() });
		data.add(new String[] { "Motd", t.getMotd() });
		data.add(new String[] { "Slots",
				t.getOnlinePlayers() + "/" + t.getMaxPlayers() });
		if ((t.getPlayers() != null ? t.getPlayers().length() : 0) > 0) {
			data.add(new String[] { "Players List", t.getPlayers() });
		}
		data.add(new String[] { "Ping", t.getLatency() + "ms" });
		data.add(new String[] { "Uptime",
				((double) times_online / (double) times_checked) + "%" });
		data.add(new String[] { "Status", (t.isOnline() ? "Online" : "Offline") });
		if ((t.getVersion() != null ? t.getVersion().length() : 0) > 0) {
			data.add(new String[] { "Version", t.getVersion() });
		}

		lv = (ListView) findViewById(R.id.server_data);
		adapter = new ServerInfoAdapter(data, this);
		lv.setAdapter(adapter);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int pos,
					long id) {
				// View current = adapter.getView(pos, null, null);
				if (pos == 3
						&& (t.getGs4Port() > 0 || t.getMinequeryPort() > 0)) {
					SudoMethod sm = new SudoMethod("loadFaces");
					Map<String, String> articleParams = new HashMap<String, String>();
					FlurryAgent.logEvent("Faces_Loaded");
					sm.setProgMax(ViewInfoActivity.this.players.split(", ").length);
					sm.execute(ViewInfoActivity.this.players.split(", "));
				}
			}
		});
	}

	protected void delete() {
		deleteServerDialog = new AlertDialog.Builder(this)
				.setTitle("Confirm Delete")
				.setMessage(
						"Are you sure you want to delete this?  All data collected from this server will be lost.")
				.setNeutralButton("I'm Sure!",
						new AlertDialog.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								mApp = (TaskManagerApplication) getApplication();
								mApp.delTasks(id);
								Toast.makeText(getBaseContext(),
										name + " deleted", Toast.LENGTH_SHORT)
										.show();
								finish();
							}
						})
				.setNegativeButton("No", new AlertDialog.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						deleteServerDialog.cancel();
					}
				}).create();
		deleteServerDialog.show();
	}

	public void repollButtonClickHandler(View target) {
		if (isInternetAvavilable()) {
			SudoMethod sm = new SudoMethod("repoll");
			if (gs4_port > 0) {
				sm.execute("query", host, gs4_port + "");
			} else if (mqport > 0) {
				sm.execute("mq", host, mqport + "");
			} else {
				sm.execute("poll", host, gs4_port + "");
			}
		} else {
			Toast.makeText(getBaseContext(), "Network Unavailable",
					Toast.LENGTH_SHORT).show();
		}
	}

	public void shareButtonClickHandler(View target) {
		Map<String, String> analysisData = new HashMap<String, String>();
		analysisData.put("Server_Name", name); // Capture server name
		analysisData.put("Server_Address", host + ":" + port); // Capture server
																// address
		FlurryAgent.logEvent("sharebtn_clicked", analysisData);
		Intent sendIntent = new Intent();
		sendIntent.setAction(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_TEXT, name + " Minecraft Server - "
				+ host + (port == 25565 ? "" : port + ""));
		sendIntent.setType("text/plain");
		startActivity(Intent.createChooser(sendIntent, "Share server to..."));
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		Dialog dialog = null;

		switch (id) {
		case 0:

			dialog = new Dialog(ViewInfoActivity.this);
			dialog.setContentView(R.layout.players_list_dialog);
			dialog.setTitle("Current Online Players");
			Button closeBtn = (Button) dialog.findViewById(R.id.close_diag_btn);

			ListView dlv = (ListView) dialog.findViewById(R.id.plist_diaglv);
			closeBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dismissDialog(0);
				}
			});
			break;
		case 1:
			deleteConfirmDialog = new AlertDialog.Builder(this)
					.setTitle(R.string.unsaved_changed_title)
					.setMessage("Do you really want to lose this data?")
					.setNeutralButton(R.string.discard,
							new AlertDialog.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									finish();
								}
							})
					.setNegativeButton(R.string.cancel,
							new AlertDialog.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									ViewInfoActivity.this.deleteConfirmDialog
											.cancel();
								}
							}).create();
		}
		return dialog;
	}

	public void csUpdateUI() {
		mApp = (TaskManagerApplication) getApplication();
		t.setTimesChecked(times_checked);
		t.setTimesOnline(times_online);
		mApp.saveTask(t);
		changeData(t);
	}

	protected void changeData(Task t) {
		ArrayList<String> etc = new ArrayList<String>();
		// etc.add(t.getName());
		etc.add(t.getHost() + ":" + t.getPort());
		etc.add(t.getMotd());
		etc.add(t.getOnlinePlayers() + "/" + t.getMaxPlayers());
		if ((t.getPlayers() != null ? t.getPlayers().length() : 0) > 0) {
			etc.add(t.getPlayers());
		}
		etc.add((t.isOnline() ? t.getLatency() + " ms" : "-1 ms"));
		double uptime = (double) t.getTimesOnline()
				/ (double) t.getTimesChecked();
		uptime = uptime * 10000;
		uptime = Math.floor(uptime);
		uptime = uptime / 100;
		etc.add(uptime + "%");
		// etc.add(t.getTimesOnline() + "/" + t.getTimesChecked());
		etc.add((t.isOnline() ? "Online" : "Offline"));
		if ((t.getVersion() != null ? t.getVersion().length() : 0) > 0) {
			etc.add(t.getVersion());
		}

		adapter.changeValue(etc);
		adapter.forceReload();
	}

	public void showSpinner() {
		loadSpinner.setVisibility(View.VISIBLE);
	}

	public void hideSpinner() {
		loadSpinner.setVisibility(View.INVISIBLE);
	}

	public boolean isInternetAvavilable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		if (activeNetworkInfo != null) {
			return activeNetworkInfo.isConnected();
		} else {
			return false;
		}
	}

	private class SudoMethod extends AsyncTask<String, String, String> {
		private String[] messages = new String[] { "Connection Refused",
				"Connection Successful", "Unable to find host",
				"Query Attempt Failed", "Query Request Successful" };
		private int rmsg;
		private int pcount;
		private URL url;
		private Bitmap bitmap;
		private String method;
		protected boolean skipFaces = false;

		public SudoMethod(String str) {
			this.method = str;
		}

		public void setProgMax(int pcount) {
			this.pcount = pcount;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (this.method == "loadFaces") {
				progdialog = new ProgressDialog(ViewInfoActivity.this);
				progdialog.setProgressStyle(ProgressDialog.STYLE_SPINNER
						| ProgressDialog.STYLE_HORIZONTAL);
				progdialog.setMessage("Loading Faces...");
				progdialog.setMax(pcount);
				progdialog.setCancelable(false);
				progdialog
						.setOnKeyListener(new DialogInterface.OnKeyListener() {
							@Override
							public boolean onKey(DialogInterface dialog,
									int keyCode, KeyEvent event) {
								if (keyCode == KeyEvent.KEYCODE_BACK
										&& progdialog.isShowing()) {
									cancelTask = true;
									progdialog.dismiss();
									return true;
								}
								return false;
							}
						});
				progdialog.setButton("Cancel", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						cancelTask = true;
						progdialog.dismiss();
					}
				});
				progdialog.setButton2("Skip", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						skipFaces = true;
						progdialog.dismiss();
					}
				});
				progdialog.show();
			} else if (this.method == "repoll") {
				((ImageView) findViewById(R.id.repoll_btn))
						.setVisibility(View.INVISIBLE);
				showSpinner();
			}
		}

		@Override
		protected String doInBackground(String... params) {
			if (this.method == "loadFaces") {
				Bitmap unknownBitmap = null;
				for (String name : params) {
					if (cancelTask == false) {
						try {
							if (skipFaces == true) {
								throw new IOException();
							}
							url = new URL("https://minotar.net/avatar/" + name
									+ "/100.png");
							Bitmap bf = BitmapFactory.decodeStream(url
									.openConnection().getInputStream());

							bitmap = getRoundedCornerBitmap(bf, 7);
						} catch (NullPointerException e) {
							if (unknownBitmap == null) {
								unknownBitmap = getRoundedCornerBitmap(
										BitmapFactory.decodeResource(
												getResources(),
												R.drawable.unknown_face), 7);
							}
							bitmap = unknownBitmap;
						} catch (MalformedURLException e) {
							if (unknownBitmap == null) {
								unknownBitmap = getRoundedCornerBitmap(
										BitmapFactory.decodeResource(
												getResources(),
												R.drawable.unknown_face), 7);
							}
							bitmap = unknownBitmap;
						} catch (IOException e) {
							if (unknownBitmap == null) {
								unknownBitmap = getRoundedCornerBitmap(
										BitmapFactory.decodeResource(
												getResources(),
												R.drawable.unknown_face), 7);
							}
							bitmap = unknownBitmap;
						}
						bmArray.add(bitmap);
						publishProgress("");
					} else {
						cancel(true);
					}
				}
			} else if (this.method == "repoll") {
				if (params[0] == "poll") {
					try {
						t = sq.pollServer(t);
						online = true;
						rmsg = 1;
					} catch (IOException e) {
						online = false;
						rmsg = 0;
					}
				} else if (params[0] == "query") {
					try {
						t = sq.queryServer(t);
						rmsg = 4;
						online = true;
					} catch (UnknownHostException e) {
						online = false;
						rmsg = 2;
					} catch (IOException e) {
						online = false;
						rmsg = 3;
					}
				} else if (params[0] == "mq") {
					try {
						t = sq.queryMq(t);
						rmsg = 4;
						online = true;
					} catch (UnknownHostException e) {
						online = false;
						rmsg = 2;
					} catch (IOException e) {
						online = false;
						rmsg = 3;
					} catch (JSONException e) {
						Toast.makeText(ViewInfoActivity.this,
								"An Unknown Error Has Occured",
								Toast.LENGTH_SHORT).show();
					}
				}
			} else if (this.method == "timer") {
				for (int i = 0; i < repollfreq; i++) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					String waittime = "";
					int days = (int) Math.floor((repollfreq - i) / 86400);
					int hrs = (int) Math
							.floor((repollfreq - i - (days * 86400)) / 3600);
					int mins = (int) Math
							.floor((repollfreq - i - (hrs * 3600)) / 60);
					int secs = (repollfreq - i - (mins * 60) - (hrs * 3600) - (days * 86400));
					if (days > 0)
						waittime = waittime + days + "days ";
					if (hrs > 0)
						waittime = waittime + mins + "hrs ";
					if (mins > 0)
						waittime = waittime + mins + "min ";
					waittime = waittime + secs + "s";

					publishProgress("wait " + waittime);
				}
			}
			return "All Done!";
		}

		@Override
		protected void onProgressUpdate(String... values) {
			if (this.method == "loadFaces") {
				progdialog.incrementProgressBy(1);
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (this.method == "loadFaces") {
				ListView players_listview = new ListView(ViewInfoActivity.this);
				LayoutParams lparams = new LinearLayout.LayoutParams(
						android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
						android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 0.5f);
				lparams.setMargins(25, 0, 25, 0);
				players_listview.setLayoutParams(lparams);
				PlayersListAdapter adapter = new PlayersListAdapter(
						ViewInfoActivity.this.players.split(", "),
						ViewInfoActivity.this, bmArray);
				players_listview.setAdapter(adapter);

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						ViewInfoActivity.this);
				alertDialogBuilder.setView(players_listview);
				alertDialogBuilder.setTitle("Current Online Players");
				alertDialogBuilder
						.setCancelable(false)
						.setNeutralButton("Close",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								}).create().show();
				progdialog.dismiss();
			} else if (this.method == "repoll") {
				hideSpinner();
				Toast.makeText(getBaseContext(), messages[rmsg],
						Toast.LENGTH_SHORT).show();
				times_online = times_online + (online ? 1 : 0);
				times_checked = times_checked + 1;
				((ImageView) findViewById(R.id.repoll_btn))
						.setVisibility(View.VISIBLE);
				csUpdateUI();
			}
		}

		@Override
		protected void onCancelled() {
			cancelTask = false;
		}

		public Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
			Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
					bitmap.getHeight(), Config.ARGB_8888);
			Canvas canvas = new Canvas(output);

			final int color = 0xff424242;
			final Paint paint = new Paint();
			final Rect rect = new Rect(0, 0, bitmap.getWidth(),
					bitmap.getHeight());
			final RectF rectF = new RectF(rect);
			final float roundPx = pixels;

			paint.setAntiAlias(true);
			canvas.drawARGB(0, 0, 0, 0);
			paint.setColor(color);
			canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

			paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
			canvas.drawBitmap(bitmap, rect, rect, paint);

			return output;
		}
	}
}
