package com.ssttevee.taskmanagerapp.services;

import java.io.IOException;

import org.json.JSONException;

import com.ssttevee.taskmanagerapp.TaskManagerApplication;
import com.ssttevee.taskmanagerapp.ViewServersActivity.ResponseReceiver;
import com.ssttevee.taskmanagerapp.tasks.ServerQuery;
import com.ssttevee.taskmanagerapp.tasks.Task;

import android.app.IntentService;
import android.content.Intent;

public class RefreshServers extends IntentService {
	
	private TaskManagerApplication app;
	public static final String GETTING_MESSAGE = "position_and_id";
	public static final String RETURNING_MESSAGE = "returning_message";
	public static final String REPOLL_COOLDOWN_FINISH = "repoll_cooldown_fin";

	public RefreshServers() {
		super("RefreshServers");
	}

	@Override
	protected void onHandleIntent(Intent intent) {

		int[] i = intent.getIntArrayExtra(GETTING_MESSAGE);
        
		app = (TaskManagerApplication) getApplication();
		Task t = app.getTaskById(i[1]);
		
		ServerQuery sq = new ServerQuery();
		if(t.getGs4Port() > 0) {
			try {
				t = sq.queryServer(t);
			} catch (IOException e) {
			}
		} else if(t.getMinequeryPort() > 0) {
			try {
				t = sq.queryMq(t);
			} catch (IOException e) {
			} catch (JSONException e) {
			}
		} else {
			try {
				t = sq.pollServer(t);
			} catch (IOException e) {
			}
		}
    	
		app = (TaskManagerApplication) getApplication();
		app.saveTask(t);
        
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(ResponseReceiver.ACTION_RESP);
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        broadcastIntent.putExtra(RETURNING_MESSAGE, i);
        sendBroadcast(broadcastIntent);
	}

}
