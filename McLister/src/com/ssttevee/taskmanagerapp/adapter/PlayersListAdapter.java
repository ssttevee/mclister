package com.ssttevee.taskmanagerapp.adapter;

import java.util.ArrayList;

import com.ssttevee.taskmanagerapp.R;
import com.ssttevee.taskmanagerapp.views.plistItem;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class PlayersListAdapter extends BaseAdapter {
	
	private Context context;
	private String[] items;
	private ArrayList<Bitmap> bitmaps;


	public PlayersListAdapter(String[] item, Context context, ArrayList<Bitmap> bma) {
	    super();
		this.items = item;
		this.context = context;
		this.bitmaps = bma;
	}
	
	@Override
	public int getCount() {
		return items.length;
	}

	@Override
	public String getItem(int position) {
		return items[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		plistItem pli;
		if(convertView == null){
			pli = (plistItem)View.inflate(context, R.layout.plist_diag_item, null);
		} else {
			pli = (plistItem)convertView;
		}
	    pli.setName(items[position], bitmaps.get(position));
		return pli;
	}

}
