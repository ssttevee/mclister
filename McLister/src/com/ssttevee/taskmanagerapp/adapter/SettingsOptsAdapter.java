package com.ssttevee.taskmanagerapp.adapter;

import java.util.ArrayList;
import java.util.List;

import com.ssttevee.taskmanagerapp.R;
import com.ssttevee.taskmanagerapp.views.SettingOptItem;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class SettingsOptsAdapter extends BaseAdapter {
	
	private Context context;
	private List<String[]> items;


	public SettingsOptsAdapter(ArrayList<String[]> item, Context context) {
	    super();
		this.items = item;
		this.context = context;
		
	}
	
	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		SettingOptItem soi;
		if(convertView == null){
			soi = (SettingOptItem)View.inflate(context, R.layout.settings_opt_item, null);
		} else {
		    soi = (SettingOptItem)convertView;
		}
	    String[] p = items.get(position);
		soi.setValue(p[0], p[1]);
		soi.setRequired(p[2]);
		return soi;
	}

	public void forceReload() {
		notifyDataSetChanged();
	}

	public void changeValue(int position, String value) {
		String[] item = items.get(position);
		item[1] = value;
		
		if(value.length() == 0) {
			item[1] = "Nothing Here!";
		}
		notifyDataSetChanged();
	}

}
