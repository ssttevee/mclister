package com.ssttevee.taskmanagerapp.adapter;

import java.util.ArrayList;
import java.util.List;

import com.ssttevee.taskmanagerapp.R;
import com.ssttevee.taskmanagerapp.views.ServerInfoItem;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class ServerInfoAdapter extends BaseAdapter {
	
	private Context context;
	private List<String[]> items;


	public ServerInfoAdapter(ArrayList<String[]> item, Context context) {
	    super();
		this.items = item;
		this.context = context;
	}
	
	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public String[] getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ServerInfoItem sii;
		if(convertView == null){
			sii = (ServerInfoItem)View.inflate(context, R.layout.server_info_item, null);
		} else {
			sii = (ServerInfoItem)convertView;
		}
	    String[] p = items.get(position);
	    sii.setValue(p[0], p[1]);
		return sii;
	}

	public void forceReload() {
		notifyDataSetChanged();
	}

	public void changeValue(ArrayList<String> vals) {
		int i = 0;
		for(String s : vals){
			String[] item = getItem(i);
			item[1] = s;
			items.set(i, item);
			i++;
		}
	}
}
