package com.ssttevee.taskmanagerapp.adapter;

import java.util.ArrayList;

import com.ssttevee.taskmanagerapp.R;
import com.ssttevee.taskmanagerapp.tasks.Task;
import com.ssttevee.taskmanagerapp.views.TaskListItem;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class TaskListAdapter extends BaseAdapter {
	
	private ArrayList<Task> tasks;
	private ArrayList<Boolean[]> pollStatus;
	private Context context;
	private Typeface tf;
	private int selected;
	

	public TaskListAdapter(ArrayList<Task> tasks, Context context) {
		super();
		this.tasks = tasks;
		this.context = context;
		this.pollStatus = new ArrayList<Boolean[]>();
		for(Task t : tasks) {
			pollStatus.add(new Boolean[]{false, false});
		}
	}
	
	public void setFont(Typeface tf) {
		this.tf = tf;
	}
	
	public void setTasks(ArrayList<Task> tasks) {
		if(this.pollStatus.size() != tasks.size()) {
			this.pollStatus = new ArrayList<Boolean[]>();
			for(int i = 0; i < tasks.size(); i++) {
				this.pollStatus.add(new Boolean[]{false, false});
			}
		}
		this.tasks = tasks;
	}
	
	public ArrayList<Task> getTasks() {
		return this.tasks;
	}

	@Override
	public int getCount() {
		return tasks.size();
	}

	@Override
	public Task getItem(int position) {
		return tasks.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TaskListItem tli;
		if(convertView == null){
			tli = (TaskListItem)View.inflate(context, R.layout.task_list_item, null);
		} else {
			tli = (TaskListItem)convertView;
		}
		tli.setFont(tf);
		tli.setTask(tasks.get(position));
		if(position == selected) {
			tli.setSelected(true);
		} else {
			tli.setSelected(false);
		}
		if(pollStatus.get(position)[0] && pollStatus.get(position)[1]) {
			tli.startPolling();
		} else if(!pollStatus.get(position)[0] && pollStatus.get(position)[1]) {
			tli.finishPolling();
			pollStatus.set(position, new Boolean[]{false, false});
		} else {
			
		}
		return tli;
	}

	public void forceReload() {
		notifyDataSetChanged();
	}

	public Long[] removeCompletedTasks() {
		ArrayList<Long> completedIds = new ArrayList<Long>();
		ArrayList<Task> completedTasks = new ArrayList<Task>();
		for(Task task:tasks) {
			if(task.isOnline()) {
				completedIds.add(task.getId());
				completedTasks.add(task);
			}
		}
		tasks.removeAll(completedTasks);
		notifyDataSetChanged();
		return completedIds.toArray(new Long[]{});
	}
	
	public void keepSelected(int pos) {
		this.selected = pos;
		notifyDataSetChanged();
	}
	
	public void startPoll() {
		int i = 0;
		for(Task t : tasks) {
			pollStatus.set(i, new Boolean[]{true, true});
			i++;
		}
		notifyDataSetChanged();
	}
	
	public void endPoll(int pos) {
		pollStatus.set(pos, new Boolean[]{false, true});
		notifyDataSetChanged();
	}

}
