package com.ssttevee.taskmanagerapp;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;

public class PremiumSettingsActivity extends Activity {

	private SharedPreferences prefs;
	private int pollFreq;
	private boolean autoPoll;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.settings);
		
		viewSetup();
	}
	
	private void viewSetup() {
		prefs = this.getSharedPreferences("mclprefs", MODE_PRIVATE);
		autoPoll = prefs.getBoolean("AUTOMATIC_POLLING", false);
		pollFreq = prefs.getInt("POLL_FREQUENCY", 480);
		ToggleButton toggleBtn = (ToggleButton) findViewById(R.id.poll_toggle);
		toggleBtn.setChecked(autoPoll);
		final TextView reading = (TextView) findViewById(R.id.strut);
		int hrs = pollFreq/60;
		int mins = pollFreq-(hrs*60);
		reading.setText("Checking every "+hrs+" hrs "+mins+" mins");
		SeekBar seekBar = (SeekBar)findViewById(R.id.sname_field);
		seekBar.setMax(1440);
		seekBar.setProgress(pollFreq);
		seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			int progress = 0;
			
			public void onStopTrackingTouch(SeekBar seekBar) {
		        SharedPreferences.Editor prefsEditor = prefs.edit();
		        prefsEditor.putInt("POLL_FREQUENCY", this.progress);
				prefsEditor.commit();
			}
			   
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
			   
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				int hrs = progress/60;
				int mins = progress-(hrs*60);
				this.progress = progress;
				
				if(progress == 0) {
					mins = 1;
				}
				reading.setText("Checking every "+hrs+" hrs "+mins+" mins");
			}
		});
	}

	@Override
	public void onResume() {
		super.onResume();
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if(keyCode == KeyEvent.KEYCODE_BACK) {
		    finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
	    } else {
		    return super.onKeyDown(keyCode, event);
	    }
		return true;
	}
	
	@Override
	public void onPause() {
		super.onPause();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
	}
	
	public void onToggleClicked(View view) {
        SharedPreferences.Editor prefsEditor = prefs.edit();
	    boolean on = ((ToggleButton) view).isChecked();
	    if (on) {
	        prefsEditor.putBoolean("AUTOMATIC_POLLING", true);
	    } else {
	        prefsEditor.putBoolean("AUTOMATIC_POLLING", false);
	    }
		prefsEditor.commit();
	}
	
}
