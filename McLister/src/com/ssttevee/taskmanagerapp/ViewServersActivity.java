package com.ssttevee.taskmanagerapp;

import java.util.ArrayList;
import com.example.android.trivialdrivesample.util.IabHelper;
import com.example.android.trivialdrivesample.util.IabResult;
import com.example.android.trivialdrivesample.util.Inventory;
import com.example.android.trivialdrivesample.util.Purchase;
import com.ssttevee.taskmanagerapp.R;
import com.ssttevee.taskmanagerapp.adapter.TaskListAdapter;
import com.ssttevee.taskmanagerapp.services.RefreshServers;
import com.ssttevee.taskmanagerapp.tasks.AppRater;
import com.ssttevee.taskmanagerapp.tasks.Task;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.ListView;
import android.widget.Toast;

public class ViewServersActivity extends Activity {

	static final String TAG = "ViewServersActivity";
	static final String SKU_PREMIUM = "com.ssttevee.taskmanagerapp.upgrade";
	static final int RC_REQUEST = 984358;

	IabHelper mHelper;
	Handler mHandler;

	private ResponseReceiver receiver;
	private TaskManagerApplication mApp;
	private TaskListAdapter adapter;
	private int mSelectedServerPosition;
	private Button viewButton = null;
	private Button addButton;
	private Button refreshButton;
	private Button deleteButton;
	private Button editButton;
	private AlertDialog mDialog;
	private SharedPreferences mPreferences;
	private Dialog dialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		viewSetup();

		mPreferences = this.getSharedPreferences(
				"com.ssttevee.taskmanagerapplication.prefs",
				Context.MODE_PRIVATE);

		AppRater.app_launched(ViewServersActivity.this);

		mApp = (TaskManagerApplication) getApplication();
		adapter = new TaskListAdapter(mApp.getCurrentTasks(), this);

		String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmsJvcKwDIiGVKdR/TC8+3Yn9iZbafyFaI0oebWcrFBm6lADRLGv2ITpzLEyoMS5aZ747z3hqbJlprX/JZHUHuSDogdQPg79THYdocVCBletYP/+pUOmkkjZ3/vQnDvIVj8236pCU04Klpqp2EKkKzbH3P3uQ9JcsapR5X1JZXh9ptVaVJJxY8IpyjldaGX336agQPFU0GT+UA2pRde4M+sjl153V8wp1ONPv6HH1ByZOjr6kCXP9i2+gKue5O0/9XF+o/CwT3L1HPXV3eksZPSaEHw6mV0lBY6WASkmhMh8B4LWujXW9PY89IRfFSkzAWnPrVYr7o1saPepmBYzBlwIDAQAB";
		mHelper = new IabHelper(this, base64EncodedPublicKey);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		super.onPause();
		this.unregisterReceiver(receiver);
	}

	@Override
	public void onResume() {
		super.onResume();
		mApp.loadTasks();
		adapter.setTasks(mApp.getCurrentTasks());
		adapter.forceReload();

		resetUi();

		IntentFilter filter = new IntentFilter(ResponseReceiver.ACTION_RESP);
		filter.addCategory(Intent.CATEGORY_DEFAULT);
		receiver = new ResponseReceiver();
		registerReceiver(receiver, filter);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d(TAG, "onActivityResult(" + requestCode + "," + resultCode + ","
				+ data);

		// Pass on the activity result to the helper for handling
		if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
			// not handled, so handle it ourselves (here's where you'd
			// perform any handling of activity results not related to in-app
			// billing...
			super.onActivityResult(requestCode, resultCode, data);
		} else {
			Log.d(TAG, "onActivityResult handled by IABUtil.");
		}
	}

	// Listener that's called when we finish querying the items we own
	IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
		@Override
		public void onQueryInventoryFinished(IabResult result,
				Inventory inventory) {
			Log.d(TAG, "Query inventory finished.");
			if (result.isFailure()) {
				complain("Failed to query inventory: " + result);
				return;
			}

			Log.d(TAG, "Query inventory was successful.");

			// Do we have the premium upgrade?
			mApp.setPremium(inventory.hasPurchase(SKU_PREMIUM));
			if (mApp.isPremium()) {
				setPremiumContent();
			}
		}
	};

	// Callback for when a purchase is finished
	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
		@Override
		public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
			Log.d(TAG, "Purchase finished: " + result + ", purchase: "
					+ purchase);
			if (result.isFailure()) {
				// Oh noes!
				complain("Error purchasing: " + result);
				return;
			}

			Log.d(TAG, "Purchase successful.");

			if (purchase.getSku().equals(SKU_PREMIUM)) {
				// bought the premium upgrade!
				Log.d(TAG, "Purchase is premium upgrade. Congratulating user.");
				alert("Thank you for upgrading to premium!");
				mApp.setPremium(true);
				setPremiumContent();

				mDialog = new AlertDialog.Builder(ViewServersActivity.this)
						.setTitle("Hurrah!")
						.setMessage(
								"You have successfully upgraded McLister!  This upgrade should follow your account around, so you won't need to buy it again.")
						.setNeutralButton("OK",
								new AlertDialog.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										mDialog.cancel();
									}
								}).create();
				mDialog.show();
			}
		}
	};

	private void setPremiumContent() {
		((ImageView) findViewById(R.id.premium_btn))
				.setImageDrawable(getResources().getDrawable(R.drawable.gear));
		((ImageView) findViewById(R.id.premium_btn))
				.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent intent = new Intent(ViewServersActivity.this,
								PremiumSettingsActivity.class);
						startActivity(intent);
						overridePendingTransition(R.anim.in_from_right,
								R.anim.out_to_left);
					}
				});
	}

	private void viewSetup() {
		mHandler = new Handler();
		mApp = (TaskManagerApplication) getApplication();
		mPreferences = getSharedPreferences("mclprefs", MODE_PRIVATE);

		mHandler.post(new Runnable() {
			@Override
			public void run() {
				LayoutInflater inflater = getLayoutInflater();
				Typeface tf = Typeface.createFromAsset(getAssets(),
						"fonts/volter.ttf");

				RelativeLayout mainDisplay = (RelativeLayout) inflater.inflate(
						R.layout.activity_view_servers, null);

				viewButton = (Button) mainDisplay
						.findViewById(R.id.more_info_btn);
				addButton = (Button) mainDisplay.findViewById(R.id.add_btn);
				editButton = (Button) mainDisplay.findViewById(R.id.edit_btn);
				deleteButton = (Button) mainDisplay
						.findViewById(R.id.delete_btn);
				refreshButton = (Button) mainDisplay
						.findViewById(R.id.refresh_btn);

				viewButton.setTypeface(tf);
				addButton.setTypeface(tf);
				editButton.setTypeface(tf);
				deleteButton.setTypeface(tf);
				refreshButton.setTypeface(tf);

				adapter.setFont(Typeface.createFromAsset(getAssets(),
						"fonts/volter.ttf"));
				((ListView) mainDisplay.findViewById(R.id.servers_list))
						.setAdapter(adapter);
				((ListView) mainDisplay.findViewById(R.id.servers_list))
						.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> arg0,
									View v, int pos, long id) {
								if (mSelectedServerPosition == pos) {
									viewServerButtonOnClickHandler();
								} else {
									mSelectedServerPosition = pos;
									adapter.keepSelected(mSelectedServerPosition);
									viewButton.setEnabled(true);
									editButton.setEnabled(true);
									deleteButton.setEnabled(true);
								}
							}
						});

				setContentView(mainDisplay);

				if (mApp.isPremium()) {
					startAutoPolling();
					setPremiumContent();
					((ImageView) mainDisplay.findViewById(R.id.premium_btn))
							.setImageDrawable(getResources().getDrawable(
									R.drawable.gear));
				}

				mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
					@Override
					public void onIabSetupFinished(IabResult result) {
						Log.d(TAG, "Setup finished.");

						if (!result.isSuccess()) {
							// Oh noes, there was a problem.
							complain("Problem setting up in-app billing: "
									+ result);
							return;
						}

						// Hooray, IAB is fully set up. Now, let's get an
						// inventory of stuff we own.
						Log.d(TAG, "Setup successful. Querying inventory.");
						mHelper.queryInventoryAsync(mGotInventoryListener);
					}
				});
			}
		});
	}

	public void onGenericButtonClick(View v) {
		Task t;
		Intent intent;
		switch (v.getId()) {
		case R.id.delete_btn:
			t = adapter.getItem(mSelectedServerPosition);
			mDialog = new AlertDialog.Builder(ViewServersActivity.this)
					.setTitle("Confirm Delete")
					.setMessage(
							"Are you sure you want to delete "
									+ t.getName()
									+ "?  All data collected from this server will be lost.")
					.setPositiveButton("I'm Sure!",
							new AlertDialog.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									Task t = adapter
											.getItem(mSelectedServerPosition);
									mApp.delTasks(t.getId());
									mApp.loadTasks();
									adapter.setTasks(mApp.getCurrentTasks());
									adapter.forceReload();

									Toast.makeText(getBaseContext(),
											t.getName() + " deleted",
											Toast.LENGTH_SHORT).show();

									resetUi();
								}
							})
					.setNegativeButton("No", new AlertDialog.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							mDialog.cancel();
						}
					}).create();
			mDialog.show();
			break;

		case R.id.edit_btn:
			intent = new Intent(ViewServersActivity.this,
					AddServerActivity.class);
			t = adapter.getItem(mSelectedServerPosition);
			intent.putExtra("id", t.getId());
			intent.putExtra("name", t.getName());
			intent.putExtra("host", t.getHost());
			intent.putExtra("motd", t.getMotd());
			intent.putExtra("max_players", t.getMaxPlayers());
			intent.putExtra("online_players", t.getOnlinePlayers());
			intent.putExtra("players", t.getPlayers());
			intent.putExtra("latency", t.getLatency());
			intent.putExtra("gs4_port", t.getGs4Port());
			intent.putExtra("port", t.getPort());
			intent.putExtra("mqport", t.getMinequeryPort());
			intent.putExtra("version", t.getVersion());
			intent.putExtra("times_online", t.getTimesOnline());
			intent.putExtra("times_checked", t.getTimesChecked());
			intent.putExtra("online", t.isOnline());
			startActivity(intent);
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			break;

		case R.id.refresh_btn:
			if (isInternetAvavilable()) {
				ArrayList<Task> at = adapter.getTasks();
				int i = 0;
				for (Task task : at) {
					intent = new Intent(
							"com.ssttevee.taskmanagerapp.services.RefreshServers");
					intent.putExtra(RefreshServers.GETTING_MESSAGE, new int[] {
							i, (int) task.getId() });
					startService(intent);
					i++;
				}
				adapter.startPoll();
			} else {
				Toast.makeText(getBaseContext(), "Network Unavailable",
						Toast.LENGTH_SHORT).show();
			}
			break;

		case R.id.add_btn:
			if (isInternetAvavilable()) {
				intent = new Intent(this, AddServerActivity.class);
				intent.putExtra("id", (long) 65535);
				intent.putExtra("name", "");
				intent.putExtra("host", ":25565");
				intent.putExtra("motd", "");
				intent.putExtra("max_players", 0);
				intent.putExtra("online_players", 0);
				intent.putExtra("players", "");
				intent.putExtra("latency", 0);
				intent.putExtra("gs4_port", 0);
				intent.putExtra("version", "");
				intent.putExtra("times_online", 0);
				intent.putExtra("times_checked", 0);
				intent.putExtra("online", false);
				startActivity(intent);
				overridePendingTransition(R.anim.in_from_right,
						R.anim.out_to_left);
			} else {
				Toast.makeText(getBaseContext(), "Network Unavailable",
						Toast.LENGTH_SHORT).show();
			}
			break;

		case R.id.more_info_btn:
			viewServerButtonOnClickHandler();
			break;

		default:
			break;
		}
	}

	public void onPremiumButtonClicked(View v) {
		if (mApp.isPremium()) {
			Intent intent = new Intent(ViewServersActivity.this,
					PremiumSettingsActivity.class);
			startActivity(intent);
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
		} else {
			dialog = new Dialog(ViewServersActivity.this);
			dialog.setContentView(R.layout.upgrade_dialog);
			dialog.setTitle("Upgrade McLister!");

			Button closeBtn = (Button) dialog.findViewById(R.id.close_button);
			Button upgradeBtn = (Button) dialog.findViewById(R.id.upgra_button);
			closeBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});
			upgradeBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mHelper.launchPurchaseFlow(ViewServersActivity.this,
							SKU_PREMIUM, RC_REQUEST, mPurchaseFinishedListener);
				}
			});

			dialog.show();
		}
	}

	private void startAutoPolling() {
		if (!isServiceRunning("com.ssttevee.taskmanagerapp.services.PeriodicPoll")) {
			startService(new Intent(
					"com.ssttevee.taskmanagerapp.services.PeriodicPoll"));
		}
	}

	private void resetUi() {
		mSelectedServerPosition = -1;
		adapter.keepSelected(mSelectedServerPosition);
		if (viewButton != null) {
			viewButton.setEnabled(false);
			editButton.setEnabled(false);
			deleteButton.setEnabled(false);
		}
	}

	protected void viewServerButtonOnClickHandler() {
		if (mSelectedServerPosition >= 0) {
			Task t = adapter.getItem(mSelectedServerPosition);
			mApp.setCurrentTask(t);

			Intent intent = new Intent(this, ViewInfoActivity.class);
			intent.putExtra("id", t.getId());
			startActivity(intent);
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
		}
	}

	protected void sendPopup(String msg) {
		mDialog = new AlertDialog.Builder(this)
				.setTitle(R.string.unsaved_changed_title)
				.setMessage(msg)
				.setPositiveButton(R.string.add_task,
						new AlertDialog.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
							}
						})
				.setNeutralButton(R.string.discard,
						new AlertDialog.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								finish();
							}
						})
				.setNegativeButton(R.string.cancel,
						new AlertDialog.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								mDialog.cancel();
							}
						}).create();
		mDialog.show();
	}

	public boolean isInternetAvavilable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		if (activeNetworkInfo != null) {
			return activeNetworkInfo.isConnected();
		} else {
			return false;
		}
	}

	public boolean isServiceRunning(String serviceClassName) {
		ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if (serviceClassName.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	public class ResponseReceiver extends BroadcastReceiver {
		public static final String ACTION_RESP = "com.ssttevee.intent.action.SERVER_POLLED";

		@Override
		public void onReceive(Context context, Intent intent) {
			int[] i = intent.getIntArrayExtra(RefreshServers.RETURNING_MESSAGE);
			mApp.loadTasks();
			adapter.setTasks(mApp.getCurrentTasks());
			adapter.endPoll(i[0]);
		}
	}

	void complain(String message) {
		Log.e(TAG, "**** McLister Error: " + message);
		// alert("Error: " + message);
	}

	void alert(String message) {
		AlertDialog.Builder bld = new AlertDialog.Builder(this);
		bld.setMessage(message);
		bld.setNeutralButton("OK", null);
		Log.d(TAG, "Showing alert dialog: " + message);
		bld.create().show();
	}
}
