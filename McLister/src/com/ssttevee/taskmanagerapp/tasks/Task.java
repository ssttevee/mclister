package com.ssttevee.taskmanagerapp.tasks;

public class Task {

	private long id;
	private String name;
	private String host;
	private String motd;
	private int max_players;
	private int port;
	private int online_players;
	private String players;
	private int latency;
	private int gs4_port;
	private int minequery_port;
	private String version;
	private int times_online;
	private int times_checked;
	private boolean online;

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getMotd() {
		return motd;
	}

	public void setMotd(String motd) {
		this.motd = motd;
	}

	public int getMaxPlayers() {
		return max_players;
	}

	public void setMaxPlayers(int max_players) {
		this.max_players = max_players;
	}

	public int getOnlinePlayers() {
		return online_players;
	}

	public void setOnlinePlayers(int online_players) {
		this.online_players = online_players;
	}

	public String getPlayers() {
		return players;
	}

	public void setPlayers(String players) {
		this.players = players;
	}

	public int getLatency() {
		return latency;
	}

	public void setLatency(int latency) {
		this.latency = latency;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public int getTimesOnline() {
		return times_online;
	}

	public void setTimesOnline(int times_online) {
		this.times_online = times_online;
	}

	public int getTimesChecked() {
		return times_checked;
	}

	public void setTimesChecked(int times_checked) {
		this.times_checked = times_checked;
	}

	public boolean isOnline() {
		return online;
	}

	public void setOnline(boolean online) {
		this.online = online;
	}

	public Task(String host, int gs4Port) {
		this.host = host;
		this.gs4_port = gs4Port;
		this.online = false;
	}

	public Task(long id, String name, String host, String motd, int pmax, int ponline, String players, int ping, String version, int tonline, int tchecked, boolean online) {
		this.id = id;
		this.name = name;
		this.host = host;
		this.motd = motd;
		this.max_players = pmax;
		this.online_players = ponline;
		this.players = players;
		this.latency = ping;
		this.version = version;
		this.times_online = tonline;
		this.times_checked = tchecked;
		this.online = online;
	}

	public Task(String host) {
		this.host= host;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return name;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public long getId() {
		return id;
	}

	public int getGs4Port() {
		return gs4_port;
	}

	public void setGs4Port(int gs4_port) {
		this.gs4_port = gs4_port;
	}

	public int getMinequeryPort() {
		return minequery_port;
	}

	public void setMinequeryPort(int minequery_port) {
		this.minequery_port = minequery_port;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
}
