package com.ssttevee.taskmanagerapp.tasks;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.lang.Math;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.util.Log;

public class ServerQuery {
	
	private String motd;
	private int maxPlayers;
	private int playerCount;
	private String players;
	private int latency;
	private String version;
	private int times_online;
	private int times_checked;
	private boolean online;
	private static InetAddress ipAddr;
	private static DatagramSocket dp;
	private static DatagramPacket dpr;
	private boolean addrValid;
	public final static String STATISTIC = "\u0000";
	public final static String HANDSHAKE = "\u0009";
	
	public static final String allowedCharacters = getAllowedCharacters();

	public String getMotd() {
		return motd;
	}

	public void setMotd(String motd) {
		this.motd = motd;
	}

	public int getMaxPlayers() {
		return maxPlayers;
	}

	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = maxPlayers;
	}

	public int getPlayerCount() {
		return playerCount;
	}

	public void setPlayerCount(int playerCount) {
		this.playerCount = playerCount;
	}

	public String getPlayers() {
		return players;
	}

	public void setPlayers(String players) {
		this.players = players;
	}

	public int getLatency() {
		return latency;
	}

	public void setLatency(int latency) {
		this.latency = latency;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public int getTimesOnline() {
		return times_online;
	}

	public void setTimesOnline(int times_online) {
		this.times_online = times_online;
	}

	public void addOnlineTally() {
		this.times_online = this.times_online + 1;
	}

	public int getTimesChecked() {
		return times_checked;
	}

	public void setTimesChecked(int times_checked) {
		this.times_checked = times_checked;
	}

	public void addCheckedTally() {
		this.times_checked = this.times_checked + 1;
	}

	public boolean isOnline() {
		return online;
	}

	public void setOnline(boolean online) {
		this.online = online;
	}
	
	public Task queryMq(Task t) throws IOException, UnknownHostException, JSONException {
        Socket socket = null;
        PrintWriter out = null;
        BufferedReader in = null;
        BufferedReader read = null;
        JSONObject object = null;
 
        try {
            t.setTimesChecked(t.getTimesChecked() + 1);
            long startTime = System.nanoTime();
            socket = new Socket(t.getHost(), t.getMinequeryPort());
            float estimatedTime = (System.nanoTime() - startTime) / 1000000;
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out.println("QUERY_JSON");
            read = new BufferedReader(new InputStreamReader(System.in));
            object = (JSONObject) new JSONTokener(in.readLine()).nextValue();
            
            JSONArray players = (JSONArray) object.get("playerList");
            int len = players.length();
            String playerList = "";
            for (int i=0;i<len;i++){ 
            	if(playerList != "") {
            		playerList += ", ";
            	}
            	playerList += players.get(i);
            }
            t.setPlayers(playerList);
            t.setMaxPlayers(object.getInt("maxPlayers"));
            t.setOnlinePlayers(object.getInt("playerCount"));
            t.setPort(object.getInt("serverPort"));
            t.setLatency((int) Math.floor(estimatedTime + 0.5f));
            t.setTimesOnline(t.getTimesOnline() + 1);
            
            t = pollServer(t);
            
        } catch (UnknownHostException e) {
        	t.setMotd("Can't resolve hostname");
            t.setOnline(false);
            t.setLatency(-1);
            t.setMaxPlayers(0);
            t.setOnlinePlayers(0);
            throw e;
        } catch (IOException e) {
        	t.setPlayers("Minequery Failed");
            throw e;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Throwable throwable) { }

            try {
                if (out != null) {
                    out.close();
                }
            } catch (Throwable throwable) { }
            
            try {
            	if (read != null) {
                    read.close();
            	}
            } catch (Throwable throwable) { }

            try {
                if (socket != null) {
                    socket.close();
                }
            } catch (Throwable throwable) {
                setMotd("Can't reach server");
            }
        }
		return t;
	}
	
	public Task queryServer(Task t) throws IOException {
		InetAddress ia = parseIp(t.getHost().split(":")[0]);
		DatagramSocket dsg = initConnection(ia, t.getGs4Port());
		
		return queryServer(t, ia, dsg);
	}

    public Task queryServer(Task t, InetAddress addr, DatagramSocket dgs) throws IOException {
        t.setTimesChecked(t.getTimesChecked() + 1);
        
        ipAddr = addr;
        dp = dgs;
        
        String command = "xx\u0009xxxx";
        byte[] sender = command.getBytes();
        sender[0] = (byte)0xFE;
        sender[1] = (byte)0xFD;
        sender[command.length() - 1] = (byte)0x04;
        sender[command.length() - 2] = (byte)0x03;
        sender[command.length() - 3] = (byte)0x02;
        sender[command.length() - 4] = (byte)0x01;

        dpr = writeData(sender, ipAddr, t.getGs4Port(), 512);
        
        String s3 = new String(dpr.getData(),0,dpr.getLength());
        s3 = s3.substring(5).replaceAll( "[^\\d]", "" );

        command = "xx\u0000xxxxyyyyxxxx";

        sender = command.getBytes();
        byte[] challenge = packIt(Integer.parseInt(s3));
        
        sender[0] = (byte)254;
        sender[1] = (byte)253;
        sender[3] = (byte)0x01;
        sender[4] = (byte)0x02;
        sender[5] = (byte)0x03;
        sender[6] = (byte)0x04;
        sender[7] = challenge[0];
        sender[8] = challenge[1];
        sender[9] = challenge[2];
        sender[10] = challenge[3];
        sender[11] = (byte)0x00;
        sender[12] = (byte)0x00;
        sender[13] = (byte)0x00;
        sender[14] = (byte)0x00;

        dpr = writeData(sender, ipAddr, t.getGs4Port(), 4096);
        
        String s4 = new String(dpr.getData(),0,dpr.getLength());
        
        s4 = s4.substring(5).substring(11);
        String[] s5 = s4.split("\u0000\u0000\u0001player_\u0000\u0000");

        String[] players = s5[1].substring(0, s5[1].length() - 1).split("\u0000");
        String playerslist = "";
        for(String p : players) {
        	if(p == players[players.length - 1]) {
        		playerslist = playerslist + p;
        	} else {
    			playerslist = playerslist + p + ", ";
        	}
        }
        
        t.setPlayers(playerslist);

        String[] data = s5[0].split("\u0000");
        String[] xdata = null;
        String last = "";
        int b = 0;
        
        for(String d : data) {
			if( (b & 1) != 0 ) {
				if(last.length() > 0) {
    				last = last + "\u0000\u0000";
				}
				last = last + d;
			}
			b++;
        }
        
        xdata = last.split("\u0000\u0000");
        
        t.setMotd(xdata[0]);
        t.setVersion(xdata[3]);
        t.setOnlinePlayers(Integer.parseInt(xdata[6]));
        t.setMaxPlayers(Integer.parseInt(xdata[7]));
        t.setPort(Integer.parseInt(xdata[8]));
        t.setTimesOnline(t.getTimesOnline() + 1);
        
        t = pollServer(t);
        
        dp.close();
        return t;
    }

    public Task pollServer(Task t) throws IOException {
        String ServerAddress = t.getHost();
        String[] as = ServerAddress.split(":");

        if (ServerAddress.startsWith("[")) {
            int i = ServerAddress.indexOf("]");

            if (i > 0) {
                String s2 = ServerAddress.substring(1, i);
                String s3 = ServerAddress.substring(i + 1).trim();

                if (s3.startsWith(":") && s3.length() > 0) {
                    s3 = s3.substring(1);
                    as = new String[2];
                    as[0] = s2;
                    as[1] = s3;
                } else {
                    as = new String[1];
                    as[0] = s2;
                }
            }
        }

        if (as.length > 2) {
            as = new String[1];
            as[0] = ServerAddress;
        }
        
        ServerAddress = as[0];
        int ServerPort = as.length == 1 ? t.getPort() : parseIntWithDefault(as[1], 25565);
        Socket socket = null;
        DataInputStream datainputstream = null;
        DataOutputStream dataoutputstream = null;
        
        try {
        	socket = new Socket();
        	socket.setSoTimeout(3000);
        	socket.setTcpNoDelay(true);
        	socket.setTrafficClass(18);
            long startTime = System.nanoTime();
        	socket.connect(new InetSocketAddress(ServerAddress, ServerPort), 3000);
            float estimatedTime = (System.nanoTime() - startTime) / 1000000;
            t.setLatency((int) Math.floor(estimatedTime + 0.5f));
        	datainputstream = new DataInputStream(socket.getInputStream());
        	dataoutputstream = new DataOutputStream(socket.getOutputStream());
        	dataoutputstream.write(254);
        	dataoutputstream.write(1);

            if (datainputstream.read() != 255)
            {
                throw new IOException("Bad message");
            }

            String s4 = readString(datainputstream, 256);
            char ac[] = s4.toCharArray();

            for (int k = 0; k < ac.length; k++) {
                if (ac[k] != 167 && ac[k] != 0 && allowedCharacters.indexOf(ac[k]) < 0) {
                    ac[k] = 63;
                }
            }

            s4 = new String(ac);
            int l;
            int i1;
            String[] as1;

            if (s4.startsWith("\u00a7") && s4.length() > 1) {
                as1 = s4.substring(1).split("\u0000");

                if (parseIntWithDefault(as1[0], 0) == 1) {
                	
	                t.setMotd(as1[3]);
	                // par1ServerData.field_82821_f = MathHelper.func_82715_a(var26[1], par1ServerData.field_82821_f);
	                t.setVersion(as1[2]);
	                t.setOnline(true);
	                l = parseIntWithDefault(as1[4], 0);
	                i1 = parseIntWithDefault(as1[5], 0);
	
	                if (l >= 0 && i1 >= 0)
	                {
	                    t.setOnlinePlayers(l);
	                    t.setMaxPlayers(i1);
	                }
	                else
	                {
	                    t.setOnlinePlayers(0);
	                    t.setMaxPlayers(0);
	                }
                } else {
                    t.setVersion("???");
                    t.setMotd("Can't reach server");
                    // par1ServerData.field_82821_f = 50;
                    t.setOnlinePlayers(0);
                    t.setMaxPlayers(0);
                    t.setOnline(false);
                }
            } else {
                as1 = s4.split("\u00a7");
                s4 = as1[0];
                l = -1;
                i1 = -1;

                try
                {
                    l = Integer.parseInt(as1[1]);
                    i1 = Integer.parseInt(as1[2]);
                }
                catch (Exception e)
                {
                    ;
                }

                t.setMotd(s4);

                if (l >= 0 && i1 >= 0) {
                    t.setOnlinePlayers(l);
                    t.setMaxPlayers(i1);
                } else {
                    t.setOnlinePlayers(0);
                    t.setMaxPlayers(0);
                }

                t.setVersion("1.3");
                // par1ServerData.field_82821_f = 48;
            }
            

        } catch(UnknownHostException e) {
        	t.setMotd("Can't resolve hostname");
            t.setOnline(false);
            t.setLatency(-1);
            t.setMaxPlayers(0);
            t.setOnlinePlayers(0);
        } catch(IOException e) {
            t.setMotd("Can't reach server");
            t.setOnline(false);
            t.setLatency(-1);
            t.setMaxPlayers(0);
            t.setOnlinePlayers(0);
            throw e;
        } finally {
            try {
                if (datainputstream != null) {
                    datainputstream.close();
                }
            } catch (Throwable throwable) { }

            try {
                if (dataoutputstream != null) {
                    dataoutputstream.close();
                }
            } catch (Throwable throwable1) { }

            try {
                if (socket != null) {
                    socket.close();
                }
            } catch (Throwable throwable2) {
                setMotd("Can't reach server");
            }
        }
		return t;
    }

    private int parseIntWithDefault(String par1Str, int par2) {
        try {
            return Integer.parseInt(par1Str.trim());
        } catch (Exception exception) {
            return par2;
        }
    }
    
    public static String readString(DataInputStream par0DataInputStream, int par1) throws IOException {
        short word0 = par0DataInputStream.readShort();

        if (word0 > par1) {
            throw new IOException((new StringBuilder()).append("Received string length longer than maximum allowed (").append(word0).append(" > ").append(par1).append(")").toString());
        }

        if (word0 < 0) {
            throw new IOException("Received string length is less than zero! Weird string!");
        }

        StringBuilder stringbuilder = new StringBuilder();

        for (int i = 0; i < word0; i++) {
            stringbuilder.append(par0DataInputStream.readChar());
        }

        return stringbuilder.toString();
    }

    private static String getAllowedCharacters() {
        String s = " !#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[]^_'abcdefghijklmnopqrstuvwxyz{|}~⌂ÇüéâäàåçêëèïîìÄÅÉæÆôöòûùÿÖÜø£Ø×ƒáíóúñÑªº¿®¬½¼¡«»";

        s = (new StringBuilder()).append(s).append('"').toString();
        s = (new StringBuilder()).append(s).append('\\').toString();

        return s;
        
    }
    
	public void validateAddress(final String addr) {
		setAddrValid(false);
		new Thread(new Runnable(){
			@Override
			public void run() {
				try {
					InetAddress.getByName(addr);
					setAddrValid(true);
					Log.d("ServerQuery","Valid");
				} catch (UnknownHostException e) {
					setAddrValid(false);
					Log.d("ServerQuery","invalid");
				}
			}
		}).start();
	}
    
    private static byte[] packIt(int j) {
        int x = (int) Math.floor(j/65536);
        int y = (int) Math.floor((j-(x*65536))/256);
        int z = (int) Math.floor(j-((x*65536)+(y*256)));
        
        String str = "wxyz";
        
        byte[] f = str.getBytes();
        
        f[0] = (byte) 0;
        f[1] = (byte) x;
        f[2] = (byte) y;
        f[3] = (byte) z;
        
		return f;
    }
    
	public InetAddress parseIp(String addr) throws UnknownHostException {
		return InetAddress.getByName(addr);
	}
	
	public DatagramSocket initConnection(InetAddress addr, int port) throws SocketException {
    	dp = new DatagramSocket();
        dp.connect(addr, port);
        dp.setSoTimeout(3000);
		return dp;
	}
    
	public DatagramPacket writeData(byte[] sender, InetAddress ipAddress, int port, int buflen) throws IOException {

        DatagramPacket dps = new DatagramPacket(sender, sender.length, ipAddress, port);
		dp.send(dps);
        
        byte[] buffer = new byte[buflen];
        dps = new DatagramPacket(buffer,buffer.length);
		dp.receive(dps);
        
        return dps;
	}

	public boolean isAddrValid() {
		return addrValid;
	}

	public void setAddrValid(boolean addrValid) {
		this.addrValid = addrValid;
	}
}
