package com.ssttevee.taskmanagerapp.tasks;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import org.json.JSONObject;
import org.json.JSONTokener;

import android.content.Context;
import android.provider.Settings.Secure;
import android.util.Log;

public class ServerCommunicator {
	private Context context;
	private String androidId;
	
	public ServerCommunicator(Context context) {
		this.context = context;
		this.androidId = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID); 
		if(this.androidId == null) this.androidId = "something";
	}
	
//	public boolean storeUpgradedDevice(Transaction t) {
//
//		SharedPreferences myPrefs = context.getSharedPreferences("mclprefs", Context.MODE_PRIVATE);
//        SharedPreferences.Editor prefsEditor = myPrefs.edit();
//		Hashtable<String, String> h = new Hashtable<String, String>();
//		h.put("udid", androidId);
//		h.put("payload", (t.developerPayload != null ? t.developerPayload : "unavailable"));
//		h.put("time", t.purchaseTime + "");
//		h.put("product", t.productId);
//		h.put("orderid", t.orderId);
//		h.put("country", this.context.getResources().getConfiguration().locale.getDisplayCountry());
//		
//		try {
//			String result = POST("http://droid.ssttevee.com/collect/purchase_data.php", h);
//			Log.d("PostData", result);
//			
//			if(result == "OK\n") {
//		        prefsEditor.putString("SERVER_NOTIFIED", "true");
//				prefsEditor.commit();
//				return true;
//			} else {
//		        prefsEditor.putString("SERVER_NOTIFIED", "false");
//				prefsEditor.commit();
//				return false;
//			}
//		} catch (Exception e) {
//	        prefsEditor.putString("SERVER_NOTIFIED", "false");
//			prefsEditor.commit();
//			Log.e("PostData", e.getMessage()+"");
//			return false;
//		} finally {
//		}
//		
//	}
	
	public ArrayList<String> requestImport(String serviceId, String serverId) {
        ArrayList<String> arr = new ArrayList<String>();
		try {
			String result = getContents("http://droid.ssttevee.com/getfrom/"+serviceId+".php?sid="+serverId);
			if(result.indexOf("404") != -1) {
				return arr;
			}
            JSONObject obj = (JSONObject) new JSONTokener(result).nextValue();
            arr.add(obj.getString("server_name"));
            arr.add(obj.getString("server_address"));
            arr.add(obj.getString("server_port"));
            arr.add(obj.getString("times_online"));
            arr.add(obj.getString("times_checked"));
			return arr;
		} catch (Exception e) {
			Log.e("PostData", e.getMessage());
			return arr;
		}
	}
	
	public boolean checkLicence(String productId) {
		
		Hashtable<String, String> h = new Hashtable<String, String>();
		h.put("udid", androidId);
		h.put("productId", productId);
		
		try {
			String result = POST("http://droid.ssttevee.com/distrib/checkDeviceLicence.php", h);
			Log.d("PostData", result);
			
			return Boolean.parseBoolean(result.trim());
		} catch (Exception e) {
			Log.e("PostData", e.getMessage());
			return false;
		}
	}
	
	public boolean postAddedServer(Task t) {
		
		Hashtable<String, String> h = new Hashtable<String, String>();
		h.put("udid", androidId);
		h.put("sname", t.getName());
		h.put("shost", t.getHost());
		h.put("country", this.context.getResources().getConfiguration().locale.getDisplayCountry());
		
		try {
			String result = POST("http://droid.ssttevee.com/collect/servers.php", h);
			Log.d("PostData", result);
			
			if(result == "OK") {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			Log.e("PostData", e.getMessage() + "");
			return false;
		}
	}
	
	public String getContents(String url) {
        URL u;
        StringBuilder builder = new StringBuilder();
        try {
            u = new URL(url);
            try {
                BufferedReader theHTML = new BufferedReader(new InputStreamReader(u.openStream(), "UTF8"));
                String thisLine;
                while ((thisLine = theHTML.readLine()) != null) {
                    builder.append(thisLine).append("\n");
                } 
            } 
            catch (Exception e) {
                System.err.println(e);
            }
        } catch (MalformedURLException e) {
            System.err.println(url + " is not a parseable URL");
            System.err.println(e);
        }
        return builder.toString();
    }
	
	public static String POST(String targetURL, Hashtable<String, String> contentHash) throws Exception {	 
		URL url;
		URLConnection conn;
		DataOutputStream out;
		DataInputStream in;
		url = new URL (targetURL); 
		conn = url.openConnection();
		conn.setDoInput (true); 
		conn.setDoOutput (true); 
		conn.setUseCaches (false);
		conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		
		out = new DataOutputStream (conn.getOutputStream ());
		String content = "";
		
		Enumeration<String> e = contentHash.keys();
		boolean first = true;
		while(e.hasMoreElements()) 
		{
			Object key = e.nextElement(); 
			Object value = contentHash.get(key); 
			
			if(!first) {
				content += "&";
			}
			
			content += (String)key + "=" + URLEncoder.encode((String)value);
			
			first = false;
		}
		
		out.writeBytes (content); 
		out.flush (); 
		out.close (); 
		
		in = new DataInputStream (conn.getInputStream ()); 

		String returnString = "";
		String str;
		while (null != (str = in.readLine())) {
			returnString += str + "\n";
		}
		
		in.close ();
		
		return returnString;
	} 

}
