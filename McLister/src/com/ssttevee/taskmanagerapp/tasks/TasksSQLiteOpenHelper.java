package com.ssttevee.taskmanagerapp.tasks;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TasksSQLiteOpenHelper extends SQLiteOpenHelper {
	
	public static final String DB_NAME = "servers_db.sqlite";
	public static final int VERSION = 4;
	public static final String SERVERS_TABLE = "servers";
	public static final String SERV_ID = "id";
	public static final String SERV_NAME = "name";
	public static final String SERV_HOST = "host";
	public static final String SERV_PORT = "port";
	public static final String SERV_GS4 = "queryport";
	public static final String SERV_MQ = "minequery";
	public static final String SERV_MOTD = "motd";
	public static final String SERV_PMAX = "maxplayers";
	public static final String SERV_PMIN = "minplayers";
	public static final String SERV_PLAYERS = "players";
	public static final String SERV_PING = "ping";
	public static final String SERV_VERSION = "version";
	public static final String SERV_TONLINE = "onlinetimes";
	public static final String SERV_TCHECKED = "checkedtimes";
	public static final String SERV_STATUS = "online";

	public TasksSQLiteOpenHelper(Context context) {
		super(context, DB_NAME, null, VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		createTable(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
		updateTable(db);
	}

	private void createTable(SQLiteDatabase db) {
		db.execSQL(
				"create table " + SERVERS_TABLE + " (" +
				SERV_ID + " integer primary key autoincrement not null," +
				SERV_NAME + " text," +
				SERV_HOST + " text," +
				SERV_GS4 + " int," +
				SERV_MOTD + " text," +
				SERV_PMAX + " int," +
				SERV_PMIN + " int," +
				SERV_PLAYERS + " text," +
				SERV_PING + " int," +
				SERV_VERSION + " text," +
				SERV_TONLINE + " int," +
				SERV_TCHECKED + " int," +
				SERV_STATUS + " text," +
				SERV_PORT + " int," +
				SERV_MQ + " int" +
				");"
			);
	}
	
	private void updateTable(SQLiteDatabase db) {
		db.execSQL(
				"ALTER TABLE " + SERVERS_TABLE + " ADD (" +
				SERV_PORT + " int, " +
				SERV_MQ + " int" +
				");"
				);
	}

}
